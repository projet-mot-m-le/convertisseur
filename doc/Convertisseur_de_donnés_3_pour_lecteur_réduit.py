from tkinter import*
from tkinter import Label, Tk, Button, Entry, font
fen= Tk()
fen.geometry('1300x950')
fen.title("Convertisseur d'unité")
fen['bg']='#EFDFC8'
fen.resizable(False,False)

#Titre écrit sur la fenêtre + Police
title_font=font.Font(size=20,family="Cambria")
title=Label(text="Convertisseur d'unité",fg='#000000',font=title_font, bg='#EFDFC8')
title.place(x=520,y=5)

#Text explicatif +police
script_font=font.Font(size=15,family="Script Bold")
script=Label(text='Bienvenue dans votre convertisseur de données!!!:)\n Dans celui ci vous allez pouvoir convertir des données métriques.',fg='#E59866',bg='#EFDFC8')
script.place(x=470,y=50)

#Police et taille pour l'explication 1
expl_font=font.Font(size=15, family='Double Struck')
expl=Label(text="1. Entrez la valeur que vous voulez convertir", fg='#A03535',font=expl_font, bg='#EFDFC8')
expl.place(x=480,y=100)

#Barre d'entré pour convertir les données + Police
entry1_font=font.Font(size=20,family="Arial Rounded MT Bold")
entry1=Entry( width=20,borderwidth=20,font=entry1_font, bg='#BDC3C7',fg='#FFF')
entry1.place(x=480,y=150)


#Label 2 explicants les bouttons + police
expl2_font=font.Font(size=15, family='Double Struck')
expl2=Label(text="2. Choisissez l'unité de départ de votre nombre",fg='#A03535',font=expl2_font, bg='#EFDFC8' )
expl2.place(x=45, y=200)

#Text explicatif3 + Police
expl3_font=font.Font(size=15, family='Double Struck')
expl3=Label(text="4. Apppuyez sur le bouton pour convertir votre donnée ",fg='#A03535',font=expl3_font, bg='#EFDFC8',)
expl3.place(x=400,y=290)

#Text explicatif4 + police
expl3_font=font.Font(size=15, family='Double Struck')
expl4=Label(text="5. Observer votre résultat ",fg='#A03535',font=expl3_font, bg='#EFDFC8',)
expl4.place(x=500,y=470)


#Choix unité convertit + Police
expl3_font=font.Font(size=15, family='Double Struck')
expl3=Label(text="3. Choisissez l'unité final de votre nombre ",fg='#A03535',font=expl2_font, bg='#EFDFC8' )
expl3.place(x=880,y=200)

#Définition des données métriques
expo = [10**-9,10**-6,10**-3,10**-2,10**-1,10**0,10**1,10**2,10**3]
UNITE1 = ["Nanomètre","Micromètre","Milimètre","Centimètre","Décimètre","Mètre","Décamètre","Hectomètre","Kilomètre",]

#Fonction calcule de longueure
def longueur():
    try:
        expression = entry1.get().replace(',', '.')
        nbr = float(expression)
        unit_val1= unit1.get()
        unit_val2 = unit2.get()
        convert = nbr * expo[unit_val1] / expo[unit_val2]
        unit=UNITE1[unit_val2]
        result.config(text=f"Le résultat est : {convert} {unit}",fg='#3D718C'  )
    except ValueError:
        result.config(text="Erreur! Entré invalide, entrez un nombre valide ;)",fg='#A03535')
    except IndexError:
        result.config(text="Erreur! Index invalide, indiquez le bon index ;)",fg='#A03535')


#Police des boutons radio
boutp=font.Font(size=15,family="Arial Rounded MT Bold")
boutex=font.Font(size=30,family="Lucida Console")

# Boutons radio Gauche
unit1 = IntVar()
Kilo= Radiobutton(fen, text="Kilomètre", variable=unit1, value=8,font=boutp,bg="#FFDFDF")
Hecto= Radiobutton(fen, text="Hectomètre", variable=unit1, value=7,font=boutp,bg="#FFDFDF")
Déca= Radiobutton(fen, text="Décamètre", variable=unit1, value=6,font=boutp,bg="#FFDFDF")
Mètre= Radiobutton(fen, text="Mètre", variable=unit1, value=5,font=boutp,bg="#FFDFDF")
Déci= Radiobutton(fen, text="Décimètre", variable=unit1, value=4,font=boutp,bg="#FFDFDF")
Centi= Radiobutton(fen, text="Centimètre", variable=unit1, value=3,font=boutp,bg="#FFDFDF")
Mili= Radiobutton(fen, text="Milimètre", variable=unit1, value=2,font=boutp,bg="#FFDFDF")
Micro= Radiobutton(fen, text="Micromètre", variable=unit1, value=1,font=boutp,bg="#FFDFDF")
Nano= Radiobutton(fen, text="Nanomètre", variable=unit1, value=0,font=boutp,bg="#FFDFDF")

# Boutons radio Droite
unit2 = IntVar()
Kilo2= Radiobutton(fen, text="Kilomètre", variable=unit2, value=8,font=boutp, bg='#DFE5FF')
Hecto2= Radiobutton(fen, text="Hectomètre", variable=unit2, value=7,font=boutp, bg='#DFE5FF')
Déca2= Radiobutton(fen, text="Décamètre", variable=unit2, value=6,font=boutp, bg='#DFE5FF')
Mètre2= Radiobutton(fen, text="Mètre", variable=unit2, value=5,font=boutp, bg='#DFE5FF')
Déci2= Radiobutton(fen, text="Décimètre", variable=unit2, value=4,font=boutp, bg='#DFE5FF')
Centi2= Radiobutton(fen, text="Centimètre", variable=unit2, value=3,font=boutp, bg='#DFE5FF')
Mili2= Radiobutton(fen, text="Milimètre", variable=unit2, value=2,font=boutp, bg='#DFE5FF')
Micro2= Radiobutton(fen, text="Micromètre", variable=unit2, value=1,font=boutp, bg='#DFE5FF')
Nano2= Radiobutton(fen, text="Nanomètre", variable=unit2, value=0,font=boutp, bg='#DFE5FF')

#Emplacement des boutons radio
Kilo.place(x=45,y=250)
Hecto.place(x=45,y=300)
Déca.place(x=45,y=350)
Mètre.place(x=45,y=400)
Déci.place(x=45,y=450)
Centi.place(x=45,y=500)
Mili.place(x=45,y=550)
Micro.place(x=45,y=600)
Nano.place(x=45,y=650)

Kilo2.place(x=900,y=250)
Hecto2.place(x=900,y=300)
Déca2.place(x=900,y=350)
Mètre2.place(x=900,y=400)
Déci2.place(x=900,y=450)
Centi2.place(x=900,y=500)
Mili2.place(x=900,y=550)
Micro2.place(x=900,y=600)
Nano2.place(x=900,y=650)

#Police des boutons
boutp=font.Font(size=15,family="Arial Rounded MT Bold")
boutex=font.Font(size=30,family="Lucida Console")
#Fonction pour nettoyer la barre d'entré et le résultat
def clear():
    entry1.delete(0,END)
    result.config(text="")

#Boutton Convertir et Supprimer
Convert=Button(fen,text="Convertir",width=10,borderwidth=10,padx=35, pady=10,font=boutex, bg='#E2FFDF', activebackground='#33CC21',command=lambda:longueur())
Sup=Button(fen,text="Supprimer",width=10,borderwidth=10,padx=35, pady=10,font=boutex, bg= '#E35B5B',activebackground='#FF0000',command=lambda:clear())

#Place des bonttons sur la fenêtre
Convert.place(x=480,y=350)
Sup.place(x=480,y=580)

#Texte de résultat + Police
result_font=font.Font(size=20,family="Arial Rounded MT Bold")
result=Label(fen,text="",font=result_font,bg='#EFDFC8')
result.place(x=450,y=530)

#Boucle pour pouvoir voir la fenêtre constament
fen.mainloop()